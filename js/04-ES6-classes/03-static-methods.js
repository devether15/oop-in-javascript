class Circle {
    constructor (radius) {
        this.radius = radius;
    }

    //Instance method
    draw(){
        console.log('draw');
    }

    //Static method || The static method is avaliable on the class itself, not the object. We often use them to create utility functions that are not specific to a given object
    static parse(str){
        const radius = JSON.parse(str).radius;
        return new Circle( radius );
    }
}

//const circle = new Circle(1); // if we use static method, we don't have to create a circle object first because we don't have a cicle onject to start woth, we have a JSON string

//if we're using a static method we don't need to create a instance of the class
const circle = Circle.parse('{"radius": 1}');
console.log(circle); 

//Example 2

class Math2{ //supusing Math2 is the build in Math class and we´re implementing it because it doesn't exist
    static absolute(value){
        //... some magic
    }
}

//we can access the absolute method, without making an instance. It takes an input and return somthing.
Math2.absolute()