//METHOD OVERRIDING

//It is wehn we have a method in a base class, or a base object,
//but we want to change ist implementation in a direct class, or a direct object.

//Lets supose that the algorithm for moving a shape is equal for most of the Shapes,
//but perhaps our circles need a different algorithm to be moved, so here we can overrride 
//this move method by implementing it in the circle class.

class Shape {
    move() {
        console.log('move');
    }
}

class Circle extends Shape {
    move() {                            //this implementation will override the one on the Shape class, because of the prototypical inheritance chain
        super.move();                   //this will also call the implementation of move() method on the Shape class
        console.log('circle move');
    }
}

const c = new Circle();
console.log(c); //

// Circle {}
// __proto__: Shape
// constructor: class Circle    
// move: ƒ move()                     //this will be the one to be executed, because it's the first implementation on the prototupe chain, the implementation in the child object.
// __proto__:
// constructor: class Shape
// move: ƒ move()                     //this will not be executed, because it's the second implementation on the prototupe chain. NOTE: Unless we call super.move() on the Circle class constructor
// __proto__: Objec

c.move(); //JavaScript engine. first looks for this method, on the circle object itself