const _radius = new WeakMap(); 

class Circle {
    constructor(radius) {       
        _radius.set(this, radius); 
    }

    // getRadius() {                   //we can acces radius calling c.getRadius()
    //     return _radius.get(this);
    // }

    // Object.defineProperty(this, 'radius', {  // this how we define a property in plane javascript
    //     get: function(){
                //...                           //makig this, we can access the property using  c.radius;
    //     }
    // })

    get radius() {                              //this is the ES6 equivalence, we can access it using c.radius;  but the declaration is simplier 
        return  _radius.get(this);
    }

    set radius(value) {
        if (value <= 0) throw new Error('invalid radius');
        _radius.set(this, value);
    }
    
}

const c = new Circle(1);

c.radius = 10;
console.log(c.radius);  //10
c.radius = -1,
console.log(c.radius);  //Uncaught Error: invalid radius at Circle.set radius [as radius] (07-getters-and-setters.js:23)