// a WeakMap() is essentially a dictinoary where keys are objects and values can be anything
//and the reason we call them weakmaps is because the keys are weak, If there are no referenes
//to these keys, they will be garbage colector.

const _radius = new WeakMap(); 
const _move = new WeakMap();

class Circle {
    constructor(radius) {
        //this.radius = radius
        _radius.set(this, radius);  //we use teh set method to define the private property. the key has to be an object, it cannot be a symbol. So here we passed 'this' which represents the instance of circle object, that's our key. Ans our value is the radius argument passed on the constructor.

        // _move.set(this, function(){          //if we use a common function, this will be undefined because ES6 classes use strict mode
        _move.set(this, () => {                 //if we use arrow functions, this will be the current objectof the constructor
            console.log('move', this);
        }) //tip: arrow functions use the 'this' value of their containing funcitons.
    } 
    
    draw() {
       // console.log(_radius.get(this));

       _move.get(this)();

       console.log('draw');
    }
}

const c = new Circle(1);


//if we want to have all the weakmaps together we could do something like this:
const privateProps = new WeakMap();

class Circle2 {
    constructor(radius) {
        privateProps.set(this, {// in this object we can set all the properties and methods
            radius: radius,
            move: () => {
                console.log('move');
            }
        })   
        //if you use a single weakmap, in order to access its members, like the radius property. you will do something like this:
        console.log(privateProps.get(this).radius);
    }
    
    draw() {      
       console.log('draw');
    }
}

const c2 = new Circle2(1);