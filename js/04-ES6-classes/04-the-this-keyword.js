'use strict' //it prevents us to accidentally modifying the global object, which is a bad practice

const Circle = function() {
    this.draw = function() { console.log(this); }
}

const c = new Circle();
//method call
c.draw(); //this = Circle object

const draw = c.draw;

//Function call || calling a method as a function
draw(); //this = window object  || if we enable strict mode we get undefined instead


//the behaviour of 'this' in ES6 classes
class  Circle2 {
    draw(){
        console.log(this);
    }
}

const c2 = new Circle2();
c2.draw();                //  || tip 
const draw2 = c2.draw;
draw2();// this is undefinded || by default ES6 classes uses strict mode, whether we explicitly enable it or not