//in javascript there are two way to declare functions:

// we can call the function before it is declared, and it works because it is hoisted: which menas raised to the top
sayHello();//hello

//in contrast, functions expressions are not hoisted, we cannot use this identifier before it's declared
//sayGoodbye(); //Uncaught ReferenceError: Cannot access 'sayGoodbye' before initialization

//the same happens with a primitive 
//console.log(number);

//function declaration syntax:
function sayHello(){ 
    console.log('hello');
}

//Funtion expresion

const sayGoodbye = function(){
    //...
}; //we have to end it with a semi colon because it is an expression

const number = 1;


//Also, in javascript there are two way to declare Classes

//we cannot instantiate an object before its class declaration
const c = new Circle();//Uncaught ReferenceError: Cannot access 'Circle' before initialization

//Class declaration
class Circle {

}

//class expression
const Square = class {

}

//now unlike functions, class declaration or class expression are not hoisted