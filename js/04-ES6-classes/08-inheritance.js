class Shape {                        //abstract class
    constructor(color){
        this.color = color         // suposing all of our objects will nedd a color
    }

    move() {
        console.log('moved');
    }
}

class Circle extends Shape {        //extends keyword configure the inheritance chain, but under the hood it´s just prototypical inheritance. NOTE, with this syntax we don't have to reset the prototype and the constructor.
    constructor(color, radius){     
        super(color);               // with super we call the parent class (Shape) an bring its properties, NOTE:if we don't call super, we get this error: 08-inheritance.js:14 Uncaught ReferenceError: Must call super constructor in derived class before accessing 'this' or returning from derived constructor at new Circle
        this.radius = radius;       // this is a property of the Circle class    
    }

    draw() {
        console.log('drawn');         //methods declared here will end in the prototype of Circle
    }
}

const c = new Circle('red',1);
console.log(c); //we get this output:

// Circle {color: "red", radius: 1}
// color: "red"
// radius: 1
// __proto__: Shape
// constructor: class Circle
// draw: ƒ draw()                       // this method is declared on the Circle class, this object was created by the circle class
// __proto__:
// constructor: class Shape
// move: ƒ move()                        // this method is declared on the Shape class, this object was created by the Shape class
// __proto__: Object

//c object can we moved
c.move();
//c object can we drawn
c.draw();