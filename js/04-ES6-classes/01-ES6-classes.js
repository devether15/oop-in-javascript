// function Circle(radius) {
//     this.radius = radius;

//     this.draw = function() {
//         console.log('draw');
//     }
// }

class Circle {
    constructor(radius){
        this.radius = radius;
        this.move = function() {}; //iy you don´t want your methods to end up in the prototype, you have to declare them here in the constructor of the class
    }

    draw () { //this method is actually on the prototype, because classes are just sintactic sugar.
        console.log('draw');
    }
}

const c = new Circle(1);
console.log(c);

//this object will be like this:
// Circle {radius: 1, move: ƒ}
// move: ƒ ()
// radius: 1
// __proto__:
// constructor: class Circle
// draw: ƒ draw()
// __proto__: Object

//to prove that classes are just a layer of abstraction, but under the hood the javascript engine still use prototypical inheritance:
console.log(typeof Circle); //it outputs: function, because these classes are constructor functions