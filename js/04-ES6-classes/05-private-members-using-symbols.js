const _radius = Symbol(); //symbol is a unique identifier, it's a primitive data type
const _draw = Symbol(); 

class Circle {
    constructor(radius) {
        // this.radius = radius;
        // this['radius'] = radius;
        this[_radius] = radius;
    }

    [_draw]() { //this is a ES6 feature caled "computed property names", what we have inside of the bracket is an expression, when that expression is evaluated, the resulting value will be used as the name of a property or method, so we pass our symbol _draw and we get a unique value/identifier

    }
}

const c = new Circle(1);
console.log(c);
//we get this:
// Circle {Symbol(): 1}
// Symbol(): 1                   //this is the radus property
// __proto__:
// constructor: class Circle
// Symbol(): ƒ [_draw]()         //this is our draw method
// __proto__: Object

//a hack to access the porperty on the symbol (just for demostration, not a good practice)
console.log(Object.getOwnPropertyNames(c)); //we get an empty array [] because we don't have any properties, or more accurately, any regular properties in this object.
console.log(Object.getOwnPropertySymbols(c)); //this returns an array of symbols [Symbol()]
const key = Object.getOwnPropertySymbols(c)[0]; //we can store it and acces the first element on the array
console.log(c[key]); //we get the value of the radius property 