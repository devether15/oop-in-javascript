function Stopwatch() { 
    let duration;
    
    this.start = function() {

    };

    this.stop = function() {

    };

    this.reset = function() {

    };

    Object.defineProperty(this, 'duration', { 
        get: function() { 
            return duration; 
        },
        set: function(value) { 
            if(value !== Number)
            throw new Error('Invalid duration') 
            duration = value;
        }
    });
} 

const sw = new Stopwatch();
