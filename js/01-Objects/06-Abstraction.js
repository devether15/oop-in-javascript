function Circle(radius) {
    this.radius = radius;

    let defaultLocation = { x: 0, y: 0}; //if we declare it as a local variable it wouldn't be accesible from the outside

    let computedOptimumLocation = function() {
        //
    }

    this.draw = function() {
        computedOptimumLocation(0.1); //we can call this methos from its parent function because it's a closure.
        defaultLocation //we don´t need to use 'this.' when we are accesing a local variable from the parent function

        this.radius // but we do need 'this.' when we're accesing a property from teh parent function
        console.log(1,'draw');
    }
}

const circle = new Circle(10);
//circle. //vscode code onlie sugest property radius and method draw() of teh circle object, because the other ar private