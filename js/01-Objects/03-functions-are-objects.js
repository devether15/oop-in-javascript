function Circle(radius) {
    this.radius = radius;
    this.draw = function() {
        console.log('draw');
    }
}
console.log(Circle.constructor); //ƒ Function() { [native code] }
const oneCircle = new Circle(1);
console.log(oneCircle)//

/*************************************************/
/***Understanding bult in constructor Function****/
/*************************************************/

//this is what javascript did internally when we declare de Circle constructor on line 1
const Circle1 = new Function('radius', `
    this.radius = radius;
    this.draw = function() {
        console.log('draw');
    }
`);

const anotherCircle = new Circle1(1);
console.log(anotherCircle); // outputs a an instance of Circle1
/******************************************************/

//We can also instantiate an object like this:

Circle.call({}, 1)//this expression is exactly like the one on line 8 (const oneCircle = new Circle(1);)
Circle.apply({}, [1, 2, 3])//is the same as the method call, but it expect an array as second parameter


