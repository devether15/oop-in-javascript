function Circle(radius) {
    this.radius = radius;
    this.draw = function() {
        console.log(1,'draw');
    }
}

const circle = new Circle(10);

circle.location = { x: 1 };
console.log(2, circle);

//we can also access a property using bracket notation
const propertyName = 'location1';
circle[propertyName] = { x: 5 };
console.log(3, circle);

//deleting properties

delete circle.location1;
delete circle.radius;
console.log(4, circle);

//lesson 9: Enumerating properties
for (let key in circle) {
    if (typeof circle[key] !== 'function')
        console.log(key, circle[key]);
}

const keys = Object.keys(circle);
console.log(keys);

if ('radius' in circle)
console.log('Circle has a radius');