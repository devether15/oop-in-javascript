//primitive types
let x = 10;
let y = x;

x = 20;

//they are independent from each other
console.log(x);
console.log(y);

//reference types (or objects)
let x1 = { value: 20};
let y1 = 20;

//they are not independent
console.log(x1);
console.log(y1);

//in conclusion: Primitives are copied by their value and objects are 
//copied by their reference

//another example (primitive type)

let number = 15;

function increase(number) {
    number++
}

increase(number);
console.log(number); //15 instead of 16


// reference type
let obj = { value: 15};

function increase(obj) {
    obj.value++
}

increase(obj);
console.log(obj); //16 it points to the same variable