function Circle(radius) {
    this.radius = radius;

    let defaultLocation = { x: 0, y: 0}; //if we declare it as a local variable it wouldn't be accesible from the outside

    this.getDeafultLocation = function() { // this method allow us to access the private propertu (technically a local variable)
        return defaultLocation; //this is a closure
    };

    this.draw = function() {         
        console.log('draw');
    };

    //the defineProperty method form the global Object, allow us to define private properties of the circle object
    Object.defineProperty(this, 'defaulLocation', { //we have to pass it three parameters, the object to modify, the name of the property and third is and object with a key valur pais
        get: function() { //the key get is an especial keyword for javascript, and the value is a function, whe we call circle.defaulLocation, this function will be called.
            return defaultLocation; //this variable is part of the closure of the inner function
        },
        set: function(value) { // set is also a special keyword in javascript and the value is a function, wich is the getter, it takes an argument called "value"
            if(!value.x || !value.y) //baceuse it's a function we can perform validations
            throw new Error('Invalid location') //this is a built in constructor so we can use to create Error objects
            defaultLocation = value; // we set the property to the value bringed in the parameter
        }
    });  
}

const circle = new Circle(10);
circle.defaultLocation = 1; //this will throw an invalid location error
circle.draw();

//if we type circle on the browser's console, we can see that defaultLocation is a computed property : defaulLocation: (...) , and when we click the dots, the get method will be executed and the values will be shown.
