function Shape(color) {
    this.color = color;
  }
  
Shape.prototype.duplicate = function() {
    console.log('duplicate');
}

//this is the intermediate function, we encansulated the logic for calling the super
function extend(Child, Parent) {
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
}

function Circle(radius, color) {
    Shape.call(this, color);//this is the super constructor, we need to use it to have the prototypical inheritance from the Shape Object. (if we don't use it, we wont have the properties of Shape object, they will be loaded on the window or global object (objectBase));

    this.radius = radius;
}

extend(Circle, Shape);

Circle.prototype.draw = function() {
    console.log('draw');
}

const s = new Shape();
const c = new Circle(1,'blue');

function Square (size) {
    this.size = size;
}

//   //resetting the prototype and constructor
//   Square.prototype = Object.create(Shape.prototype);   //thisis noisy, we can refactor this to a reusable function
//   Square.prototype.constructor = Square;
  
extend(Square, Shape); //this is the callinf of the intermediate function inheritance.

const sq = new Square(10);
sq.duplicate(); //Squeare has the duplicate() method, defined on the Shape object

