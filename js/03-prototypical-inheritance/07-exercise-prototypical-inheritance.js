//my solution
function HtmlElement(){
    this.click = function(){
        console.log('clicked');
    }
}

HtmlElement.prototype.focus = function(){
    console.log('focused');
}

const e = new HtmlElement();
console.log(e);

function HtmlSelectElement(items = []){
    this.items = items;

    this.addItem = function(item){
        this.items.push(item);
    };

    this.removeItem = function(item){
        let i = this.items.indexOf(item);

        if (i > -1) {
            this.items.splice(i, 1);
         }        
    };

    this.render = function(){
        return `
        <select>${this.items.map(item => `
            <option>${item}</option>`).join('')}
        </select>`;        
    }
}

//HtmlSelectElement.prototype = Object.create(HtmlElement.prototype); //this was my solution, my it's wrong because with Obbject.create I only inherit the focus method delcared on the prototype, but not the click method declared on the constructor.
HtmlSelectElement.prototype = new HtmlElement(); //this is the right solution, because the instance of HtmlElement has the click method because the constructor of HtmlElement object was called.
HtmlSelectElement.prototype.constructor = HtmlSelectElement; 

const s = new HtmlSelectElement();
console.log(s);


//polymorphism exercise
function HtmlImageElement(src) {
    this.src = src;

    this.render = function(){
        return `<img src="${this.src}" />`;
    }
}

HtmlImageElement.prototype = new HtmlElement();
HtmlImageElement.prototype.constructor = HtmlImageElement;

const i = new HtmlImageElement();
console.log(i);

const elements = [
    new HtmlSelectElement([1,2,3]),
    new HtmlImageElement('http://')
]

for (let element of elements)
    console.log(element.render());