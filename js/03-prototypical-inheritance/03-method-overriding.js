//overriding means re-implementing a method in a child object

function extend(Child, Parent) {
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
}

function Shape() {
}

Shape.prototype.duplicate = function() {
    console.log('duplicate');
}

function Circle() {  
}

extend(Circle, Shape);

//IMPORTANT: we have to override it after the extend, if we don't. The prototype and constructor wil be resetted and we will loose our new duplicate() implementation
Circle.prototype.duplicate = function() { //If we don't want to use duplicate method in the same way in some object, we can overwrite it
    console.log('duplicate circle');

    //sometimes we may wanna call the implementation on the parent object as well 
    Shape.prototype.duplicate.call(this);
}

const c = new Circle();

//in the prototype chain, the duplicate method declared on the child (Circle prototype) will be the first picked by the javascript engine, althought there's another on the objectBase.
console.log(c.duplicate()); //this output "duplicate circle" implemented on the Circle object and then "duplicate" wich was implementation on the Shape object. 