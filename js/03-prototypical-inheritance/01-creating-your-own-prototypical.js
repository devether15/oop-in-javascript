function Shape(color) {
  this.color = color;
}

Shape.prototype.duplicate = function() {
    console.log('duplicate');
}

function extend(Child, Parent) {
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
}

function Circle(radius, color) {
    Shape.call(this, color);//this is the super constructor, we need to use it to have the prototypical inheritance from the Shape Object. (if we don't use it, we wont have the properties of Shape object, they will be loaded on the window or global object (objectBase));

    this.radius = radius;
}

extend(Circle, Shape);

// Circle.prototype = Object.create(Shape.prototype);//before this line the prototype of the circle was the objectBase, or something like this: Circle.prototype = Object.create(Object.prototype) 

// //afeter the line 13, the circle constructor is changed to the shape constructor, so every time we wanted to change dinamically a circle ww will have an shepe object,
// //before this line, the constructor is like this: Circle.proptotype.constructor = Circle;
// //so, we could do this: new Circle.proptotype.constructor() => new Circle() and the new object will have the Circle constructor

// //to avoid that we have to reset the Circle constructor like this:
// Circle.prototype.constructor = Circle; //this is a best practice, whenever you reset the prototype of an object, yo should also reset its constructor.

//now we can new up a circle object again with the circle constreuctor, but having the duplicate() method, declare on the shape prototype
console.log(1,new Circle.prototype.constructor(1)); // we get Circle { radius: 1 }

Circle.prototype.draw = function() {
    console.log('draw');
}

function Square (size) {
    this.size = size;
}

// //resetting the prototype and constructor
// Square.prototype = Object.create(Shape.prototype);
// Square.prototype.constructor = Square;

extend(Square, Shape);

const s = new Shape();
console.log(s);//this outputs the following on the browser:
// Shape {}
// __proto__: Object

const c = new Circle(1,'blue');
console.log(c);//this outputs the following on the browser:
// Circle
// radius: 1
// __proto__: Shape
// draw: ƒ ()
// __proto__:
// duplicate: ƒ ()
// constructor: ƒ Shape()
// __proto__: Object


//conclusion, now every circle created will have Shape as its prototype, 
//and then the prototype of Shape is objectBase. Completing the prototypical inherintance.

//so now:

c.draw();//circle has the draw method defined on the Circle prototype

c.duplicate();//and also has duplicate method defined on the Shape prototype
const sq = new Square(10)
console.log(3,sq);