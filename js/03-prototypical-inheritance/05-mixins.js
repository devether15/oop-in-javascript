//This the composition aproach, wich is different to the inheritance aproach

const canEat = { //object declared with the literal object sintax
    eat: function(){
        this.hunger--,
        console.log('eating');
    }
};

//another feature declared as an object
const canWalk = {
    walk: function() {
        console.log('walking');
    }
};

function Person(){

}

//we can compose these objects together to create a person that can eat and walk. Object.assign is a ES& feature
// const person = Object.assign({}, canEat, canWalk);
// console.log(person);

//now, we added the Person constructor, we can do the same but instead of passing an empty object as the first parameters we can pass Person.prototype
//Object.assign(Person.prototype, canEat, canWalk);//first argument is the target object, the next are the ones to be loaded on target object.
//using mixin function to simplify:
mixin(Person.prototype, canEat, canWalk);//the end result will be exactly the same

//we modified the prototype of person and added the capability to eat and walk.
//so, next time we create a person object, that person will have this capabilities:
const person = new Person();
console.log(person); //in the prototype of person object we have the eat and walk methods.


//Let's continue:
//imagine tomorrow we're going to add two new objects in this application: Goldfish and Duck.
//both shoulkd have the capability to swim: 

//so we define a new feature
const canSwim = {
    swin: function(){
        console.log('swimming');
    }
};

//so now we can define a new constructor like Goldfish
function Goldfish(){

}

//and use Object.assing to modify the prototype for Goldfish:
//Object.assign(Goldfish.prototype, canEat, canSwim);

//using mixin function
mixin(Goldfish.prototype, canEat, canSwim);
const gf = new Goldfish();
console.log(gf);
//now the Goldfish prototype will look like this:
// Goldfish {}
// __proto__:
// eat: ƒ ()
// swin: ƒ ()
// constructor: ƒ Goldfish()
// __proto__: Object


//to make the code more readable, we can extract logic on the lines 26 and 53 into a function called mixin.

// let's define this function mixin, we add the target object here, and We want to have one or more sources. Now we don't want to add multiple parameters here like source1
// source2, because we don't know every time we want to use this function how many arguments we're going to pass here. So, to solve this problem,
// we can use the rest operator in ES6. So, we add only one parameter here: ...sources, and then use the rest operator which is ... (three dots),
// and this will collect all the arguments and turn them into an array. Now here we add Object.assign, they pass their target,
// now here, sources is an array []. But object.assign needs sources explicitly, we cannot pass an array here. So this time we can use the spread operator
// to spread an array into multiple arguments. So, the syntax is exactly the same, but this time we call this operator spread operator, because we are
// spreading an array into multiple objects. So, ...sources is the spread opperator.
function mixin(target, ...sources){
    Object.assign(target, ...sources)
}
