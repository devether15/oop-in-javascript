//Polymorphism: Poly means many, morph means form, soy polymorphism means many forms

function extend(Child, Parent) {
    Child.prototype = Object.create(Parent.prototype);
    Child.prototype.constructor = Child;
}

function Shape() {
    //...
}

Shape.prototype.duplicate = function() {
    console.log('duplicate');
}

function Circle() {  
}

extend(Circle, Shape); 


Circle.prototype.duplicate = function() { 
    console.log('duplicate circle');   
}

function Square() {
    //...
}

extend(Square, Shape); //just like circle, square inherits from shape

Square.prototype.duplicate = function() { //we redefine this method on the Square object.
    console.log('duplicate square');
}

//so fa we have a simply hierarchy, on the top we have the shape object, and we have to derivatives for child objects: Circle and Square.
//Each object will provide with a different implementation of the duplicate() method, declared on the Shape object.
//so we have many implementations, or many forms of the duplicated method and that's what we call Polymorphism.


//example:
const shapes = [
    new Circle(),
    new Square()
];

//depending on the type of the shape object, a different implementation or a different form of the duplicate method will be called.
for (let shape of shapes)
    shape.duplicate();

    //if shape is circle the implementation of duplicate in the circle object will be called, it outputs: duplicate circle
    //if shape is squeare a different implementation of duplicate in the square object will be called, it outputs: duplicate square
