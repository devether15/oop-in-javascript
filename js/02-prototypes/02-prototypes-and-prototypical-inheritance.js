var x = {};

var y = {};

console.log(x.toString()); //we didn't write the toString() method. x inherit it from its prototype (the global Object i this case)

console.log(1, Object.getPrototypeOf(x)); //its prototype is the global Object.

console.log(2, Object.getPrototypeOf(x) === Object.getPrototypeOf(y)); //both share its protoype, but it's just one instance of the global Object

//multilevel inheritance

var myArray = [];

console.log(myArray); //check in the browser, its prototype is the global Array and at the same time, the prototype of Array is th global Object

//CONCLUSION: Objects created by a given constructor will have the same prototype.

//ecample: all arrays created by the Array constructor, willl have same prototype