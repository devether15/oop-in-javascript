let myObj = {}; //when we use the object literal syntax. under th hood that's transalted to new Object()
Object.getPrototypeOf(myObj); // we get something like this on browser's console: myObj.__proto__ (paren of myObj)

//NOTE: constructors also has a prototype property

//We have the circle constructor function
function Circle(radius){
    this.radius = radius;
}
//remenber, functions are objects if we type Circle. the ide will suggest us the properties and methods avaliables

const circle = new Circle(1);


//example 1
let obj = {};
//this object has a prororype property which is its parent
obj.__proto__; //this is deprecated, but we can use it to trouble shot problems in the browser, in this case it returns the objectBase

//the Object constructor as a prototype property
Object.prototype //this is the object that will be used as the proptotype for all objects created by this constructor
//the sentnece from line 20 will produce the same output as the line 17, both return the objectBase

//so... obj.__proto__ is equal to Object.prototype
//the first one is an object and the second is a constructor


//example 2
let array = [];
//this array has a prototype
array.__proto__; //this returns all the methods that can be called on an array. we'll call this arrayBase

//when we use an array literal, like in line 29, under the hoood, array constructor will be callled: new Array() 

//At the same time, this constructor has a prototype property which references arrayBase
Array.prototype; //this references the same object: the arrayBase


//example 3
console.log(circle); //we have the circle object
//this circle has a prototype __proto__ : Object / it is the circleBase

//we created the circle object using circle constructor, this constructor has a prototype property, which returns circleBase
Circle.prototype;

///so... these two objects are exactly the same



