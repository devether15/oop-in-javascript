function Stopwatch() { 
    let startTime, endTime, running, duration = 0;   
  
    Object.defineProperty(this, 'duration', {
      get: function() { return duration; },
      set: function(value) { duration = value } //this was needed to modify duration property on the stop method
    });

    Object.defineProperty(this, 'startTime', {
        get: function() { return startTime; }
      });

    Object.defineProperty(this, 'endTime', {
    get: function() { return endTime; }
    });

    Object.defineProperty(this, 'runningrunning', {
    get: function() { return duration; }
    });
  }

  Stopwatch.prototype.start = function() {

    if (this.running) 
      throw new Error('Stopwatch has already started.');
    
    this.running = true; 

    this.startTime = new Date();
  };

  Stopwatch.prototype.stop = function() {
   
    if (!this.running) 
      throw new Error('Stopwatch is not started.');

    this.running = false; 
      
    this.endTime = new Date();

    const seconds = (endTime.getTime() - startTime.getTime()) / 1000;
    this.duration += seconds; 
  };

  Stopwatch.prototype.reset = function() { 
    this.startTime = null;
    this.endTime = null;
    this.running = false; 
    this.duration = 0; 
  };
  
  const sw = new Stopwatch();
  sw.duration = 10; //this is wrong, we should'nt have used proptotype in the first place, 
  //now the duration can be updated from the outside of the object. Premature optizamtion is the root of all evils.