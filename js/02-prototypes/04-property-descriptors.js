let person = { name: 'Mosh'};

console.log(1,person);

console.log(2,person.toString()); //we can access the method of the parent object/prototype

for (let key in person)
    console.log(3,key);//the only key we have is name. None of the properties and methods defined in objectBase/prototype are visible here

console.log(4, Object.keys(person))//same result, we only see the name property

//so, we can't iterate properties and methods defined in objectBase. The reason is because in Javascript,
//our properties have atributes attached to them. Sometimes these attributes prevent a property from being enumerated

//for example:
let objectBase = Object.getPrototypeOf(person);
console.log(5,objectBase);

let descriptor = Object.getOwnPropertyDescriptor(objectBase, 'toString');
console.log(6,descriptor); //this produce the following result on the browser:

    // configurable: true //that means we can delete this member if we want to
    // enumerable: false  //this is why when we iterated over we couldn't see toString method.
    // value: ƒ toString()
    // writable: true // we can overwrite this method
    // __proto__: Object


//when you create your own objects, you can send these attributes for your properties, let's see an example:
Object.defineProperty(person, 'name', {
    writable: false, 
    enumerable: true, 
    configurable: false,
});

person.name = 'Jhon'; //we try to change its name property

console.log(7,person); //It won't, we get 'Mosh' as name property

console.log(8, Object.keys(person)) //if we set enumerable to false we'll get an empty array and console.log 7 will throw an empty object

delete person.name; //we try to delete property name

console.log(9, person); //we won't be able because configurable is set to false. The person object was'nt deleted







