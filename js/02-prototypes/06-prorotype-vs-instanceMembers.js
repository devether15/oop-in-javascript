function Circle(radius) {
    this.radius = radius;

    //instance members
    this.move = function() {
        console.log('move');
    }

    // this.draw = function() {
    //     console.log('draw');
    // }
}
const c1 = new Circle(1);
const c2 = new Circle(1);


//there are two type of methods in javascript: instance members vs prototype members

//the draw method will be on the protype, with this ww will be using memory more effitiently
Circle.prototype.draw = function() {//Prototype members
    this.move();//we can call other members, if it is not a instance member i will look for the prototype member an viceversa
    console.log('draw');
}

//because of the prototipical inheritance, we still can do this
console.log(1, c1.draw());

//we can overwrite methods on the objectBase
Circle.prototype.toString = function() {
    return 'circle with radius ' + this.radius;
}

console.log( 2, c1.toString() );
