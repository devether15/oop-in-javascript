function Circle(radius) {
    //instance members
    this.radius = radius;

    this.move = function() {
        console.log('move');
    }
}

const c1 = new Circle(1);

//prototype members
Circle.prototype.draw = function() {//Prototype members
    this.move();
    console.log('draw');
}

//returns instance members
console.log(1,Object.keys(c1));

//return all members (instance + proptotype)
for (let key in c1) console.log(2, key);

//in javascripy we can call instance members as 'own''too

//another example:
console.log(c1.hasOwnProperty('radius')); //true

console.log(c1.hasOwnProperty('draw')); //false, becasue it's a prototype member






