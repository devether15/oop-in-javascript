# OOP-in-JavaScript

_Notes and exercices "hands on" from Gilbert Arévalo_ 

## Udemy 🚀

* **Course**: Object Oriented Programming in JavaScript
* **About**: Learn all about OOP, understand the most confusing parts of JavaScript and prepare for technical interviews
* **Instructor**: Mosh Hamedani
* **Available in**: https://www.udemy.com/course/javascript-object-oriented-programming/

*"JavaScript is the world's most misunderstood programming language.  ― Douglas Crockford "*

*"JavaScript doesn't have a classical object-oriented model, where you create objects from classes. In fact, JavaScript doesn't have classes at all. In JavaScript, objects inherit behavior from other objects, which we call prototypal inheritance, or inheritance based on prototypes.  ― Eric Freeman "*
